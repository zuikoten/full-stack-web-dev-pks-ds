<?php
trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi(){
        return $this->nama . " sedang " . $this->keahlian;
    }
}

trait Fight {
    public $attackPower;
    public $defencePower;

    public function serang(){

    }

    public function diserang(){

    }
}

class Elang{
    use Hewan, Fight;

    public function __construct($name, $skill = "Terbang Tinggi", $limb=2, $att=10, $def=5){
        $this->nama = $name;
        $this->keahlian = $skill;
        $this->jumlahKaki = $limb;
        $this->attackPower = $att;
        $this->defencePower = $def;

    }

    public function getInfoHewan1(){
        return "Berkaki " . $this->jumlahKaki . ", keahlian : " . $this->keahlian . ", Attack : " . $this->attackPower . ", Defence : " . $this->defencePower;
    }
    
}

class Harimau{
    use Hewan, Fight;

    public function __construct($name, $skill = "Lari Cepat", $limb=4, $att=7, $def=8){
        $this->nama = $name;
        $this->keahlian = $skill;
        $this->jumlahKaki = $limb;
        $this->attackPower = $att;
        $this->defencePower = $def;

    }

    public function getInfoHewan2(){
        return "Berkaki " . $this->jumlahKaki . ", keahlian : " . $this->keahlian . ", Attack : " . $this->attackPower . ", Defence : " . $this->defencePower;
    }

}

$elang1 = new Elang("Elang Jawa");
echo $elang1->atraksi();
echo "<br>";
echo $elang1->getInfoHewan1();

echo "<br><br>";

$harimau1 = new Harimau("Harimau Sumatra");
echo $harimau1->atraksi();
echo "<br>";
echo $harimau1->getInfoHewan2();

?>